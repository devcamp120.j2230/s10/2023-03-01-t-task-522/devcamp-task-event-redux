import { TASK_BUTTON_CLICKED, TASK_INPUT_CHANGE, TASK_TOGGLE_CLICKED } from "../constants/task.constant";

const initialState = {
    inputString: "",
    taskList: []
}

const taskReducer = (state = initialState, action) => {
    switch (action.type) {
        case TASK_INPUT_CHANGE:
            state.inputString = action.payload;
            break;
        case TASK_BUTTON_CLICKED:
            if(state.inputString) {
                state.taskList.push({
                    taskName: state.inputString,
                    status: false
                })
    
                state.inputString = "";
            }
            break;
        case TASK_TOGGLE_CLICKED:
            state.taskList[action.payload].status = !state.taskList[action.payload].status;
            break;
        default:
            break;
    }

    return {...state};
}

export default taskReducer;
