import { Button, Container, Grid, List, ListItem, TextField } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";

import { taskInputChangeAction, taskAddButtonClickAction, taskToggleClickAction } from "../actions/task.actions";

const Task = () => {
    const dispatch = useDispatch();

    const { inputString, taskList } = useSelector((reduxData) => {
        return reduxData.taskReducer;
    })

    const inputChangeHandler = (event) => {
        dispatch(taskInputChangeAction(event.target.value));
    }

    const addTaskButtonClickHandler = () => {
        dispatch(taskAddButtonClickAction());
    }

    const toggleStatusClickHandler = (index) => {
        dispatch(taskToggleClickAction(index));
    }

    return (
        <Container>
            <Grid container mt={5} alignItems="center">
                <Grid item xs={12} md={6} lg={8} sm={12}>
                    <TextField label="Nhập nội dung task tại đây" variant="outlined" fullWidth value={inputString} onChange={inputChangeHandler}/>
                </Grid>
                <Grid item xs={12} md={6} lg={4} sm={12} textAlign="center">
                    <Button variant="contained" onClick={addTaskButtonClickHandler}>Thêm task</Button>
                </Grid>
            </Grid>
            <Grid container>
                <List>
                    {taskList.map((element, index) => {
                        return <ListItem key={index} style={{color: element.status ? "green" : "red"}} onClick={() => toggleStatusClickHandler(index)} >{index + 1}. {element.taskName}</ListItem>
                    })}
                </List>
            </Grid>
        </Container>
    )
}

export default Task;