export const TASK_INPUT_CHANGE = "Sự kiên khi nhập vào input task";

export const TASK_BUTTON_CLICKED = "Sự kiện khi nhấn nút add task";

export const TASK_TOGGLE_CLICKED = "Sự kiện khi nhấn vào task sẽ làm đổi màu task";