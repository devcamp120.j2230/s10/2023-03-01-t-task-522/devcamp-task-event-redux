import { TASK_BUTTON_CLICKED, TASK_INPUT_CHANGE, TASK_TOGGLE_CLICKED } from "../constants/task.constant"

const taskInputChangeAction = (inputValue) => {
    return {
        type: TASK_INPUT_CHANGE,
        payload: inputValue
    }
}

const taskAddButtonClickAction = () => {
    return {
        type: TASK_BUTTON_CLICKED
    }
}

const taskToggleClickAction = (index) => {
    return {
        type: TASK_TOGGLE_CLICKED,
        payload: index
    }
}

export {
    taskInputChangeAction,
    taskAddButtonClickAction,
    taskToggleClickAction
}